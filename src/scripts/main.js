$(document).ready(function(){

    $('.expand-arrow').click(function() {
        $('.expand-content').show();
        $(this).hide();
    });

    $('.reduce-arrow').click(function() {
        $('.expand-content').hide();
        $('.expand-arrow').show();
    });

});
